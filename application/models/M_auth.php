<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_auth extends CI_Model {

	public function cek_user($user,$table){
		$this->db->where('username',$user);
		return $this->db->get($table);
	}

}

/* End of file M_auth.php */
/* Location: ./application/models/M_auth.php */