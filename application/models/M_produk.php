<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_produk extends CI_Model {

	function produk_pajang(){
		$this->db->select('produk.*,jenis.*,olahan.*');
		$this->db->join('jenis','jenis.id_jenis=produk.id_jenis');
		$this->db->join('olahan','olahan.id_olahan=produk.id_olahan');
		return $this->db->get('produk');
	}

	function list_produk($table){
		$this->db->select('produk.*,jenis.*,olahan.*');
		$this->db->join('jenis','jenis.id_jenis=produk.id_jenis');
		$this->db->join('olahan','olahan.id_olahan=produk.id_olahan');
		return $this->db->get($table);
	}

	function create($data,$table){
		$this->db->insert($table,$data);
	}

}

/* End of file M_produk.php */
/* Location: ./application/models/M_produk.php */