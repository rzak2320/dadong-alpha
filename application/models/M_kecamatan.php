<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_kecamatan extends CI_Model {

	function list_kecamatan($table){
		return $this->db->get($table);
	}

	function create($data,$table){
		$this->db->insert($table,$data);
	}

	function get($where,$table){
		$this->db->where($where);
		return $this->db->get($table)->row_array();
	}

	function replace($where,$data,$table){
		$this->db->where($where);
		return $this->db->update($table,$data);
	}

}

/* End of file M_kecamatan.php */
/* Location: ./application/models/M_kecamatan.php */