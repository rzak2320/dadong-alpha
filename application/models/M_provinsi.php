<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_provinsi extends CI_Model {

	function list_provinsi($table){
		return $this->db->get($table);
	}

	function create($data,$table){
		$this->db->insert($table,$data);
	}

	function get($id,$table){
		$this->db->where('id_provinsi',$id);
		return $this->db->get($table)->row_array();
	}

	function replace($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}

	function trash($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

}

/* End of file M_provinsi.php */
/* Location: ./application/models/M_provinsi.php */