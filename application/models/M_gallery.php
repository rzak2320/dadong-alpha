<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_gallery extends CI_Model {

	function list_gallery($table){
		return $this->db->get($table);
	}

	function create($data,$table){
		$this->db->insert($table,$data);
	}

	function trash($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}

}

/* End of file M_gallery.php */
/* Location: ./application/models/M_gallery.php */