<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Kopi Dadong</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="<?php echo base_url('public/images/icon.ico') ?>">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url() ?>public/front/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>public/front/css/animate.css">
    
    <link rel="stylesheet" href="<?php echo base_url() ?>public/front/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>public/front/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>public/front/css/magnific-popup.css">

    <link rel="stylesheet" href="<?php echo base_url() ?>public/front/css/aos.css">

    <link rel="stylesheet" href="<?php echo base_url() ?>public/front/css/ionicons.min.css">

    <link rel="stylesheet" href="<?php echo base_url() ?>public/front/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>public/front/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="<?php echo base_url() ?>public/front/css/flaticon.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>public/front/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>public/front/css/style.css">
  </head>
  <body>
  	<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="index.html">Kopi<small>Dadong</small></a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>
	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto" id="nav">
	          <li class="nav-item"><a href="<?php echo site_url('index') ?>" class="nav-link">Home</a></li>
	          <li class="nav-item"><a href="<?php echo site_url('produk') ?>" class="nav-link">Produk</a></li>
	          <li class="nav-item"><a href="<?php echo site_url('about') ?>" class="nav-link">About</a></li>
	          <li class="nav-item"><a href="<?php echo site_url('contact') ?>" class="nav-link">Contact</a></li>
            <li class="nav-item"><a href="<?php echo site_url('login') ?>" class="nav-link">Login</a></li>
            <li class="nav-item"><a href="<?php echo site_url('register') ?>" class="nav-link">Buat Akun</a></li>
	          <!-- <li class="nav-item cart"><a href="cart.html" class="nav-link"><span class="icon icon-shopping_cart"></span></a></li> -->
	        </ul>
	      </div>
		  </div>
	  </nav>
    <!-- END nav -->