<footer class="ftco-footer ftco-section img">
    	<div class="overlay"></div>
      <div class="container">
        <div class="row mb-5">
          <div class="col-lg-3 col-md-6 mb-5 mb-md-5">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Tentang Kopi Dadong</h2>
              <p>Kopi arabika kintamani merupakan salah satu kopi dengan cita rasa yang khas di Indonesia. Kopi Arabika tumbuh dengan subur di lereng Gunung Batur dengan konstur tanah vulkanis yang pertumbuhannya diantara ketinggian 900-1300 dpl.</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          
          <div class="col-lg-2 col-md-6 mb-5 mb-md-5">
             <div class="ftco-footer-widget mb-4 ml-md-4">
              <h2 class="ftco-heading-2">Layanan</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">Kebijakan</a></li>
                <li><a href="#" class="py-2 d-block">Jasa Pengiriman</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 mb-5 mb-md-5"></div>
          <div class="col-lg-3 col-md-6 mb-5 mb-md-5">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Have a Questions?</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon icon-map-marker"></span><span class="text">Belantih, Kintamani, Bangli, 80652</span></li>
	                <li><a href="#"><span class="icon icon-phone"></span><span class="text">(+62)81916742712</span></a></li>
	                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@dadong.id</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="<?php echo base_url() ?>public/front/js/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>public/front/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="<?php echo base_url() ?>public/front/js/popper.min.js"></script>
  <script src="<?php echo base_url() ?>public/front/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url() ?>public/front/js/jquery.easing.1.3.js"></script>
  <script src="<?php echo base_url() ?>public/front/js/jquery.waypoints.min.js"></script>
  <script src="<?php echo base_url() ?>public/front/js/jquery.stellar.min.js"></script>
  <script src="<?php echo base_url() ?>public/front/js/owl.carousel.min.js"></script>
  <script src="<?php echo base_url() ?>public/front/js/jquery.magnific-popup.min.js"></script>
  <script src="<?php echo base_url() ?>public/front/js/aos.js"></script>
  <script src="<?php echo base_url() ?>public/front/js/jquery.animateNumber.min.js"></script>
  <script src="<?php echo base_url() ?>public/front/js/bootstrap-datepicker.js"></script>
  <script src="<?php echo base_url() ?>public/front/js/jquery.timepicker.min.js"></script>
  <script src="<?php echo base_url() ?>public/front/js/scrollax.min.js"></script>
  <script src="<?php echo base_url() ?>public/front/js/main.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      // Kontrol Menu
       $('#nav a[href~="' + location.href + '"]').parents('li').addClass('active');
    })
  </script>  
  </body>
</html>