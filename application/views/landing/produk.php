<section class="home-slider owl-carousel" style="height: 450px">
	<div class="slider-item" style="background-image: url(<?php echo base_url() ?>public/front/images/bg_1.jpg); height: 450px" data-stellar-background-ratio="0.5">
      	<div class="overlay"></div>
        <div class="container">
          	<div class="row slider-text justify-content-center align-items-center">

	            <div class="col-md-7 col-sm-12 text-center ftco-animate">
	            	<h1 class="mb-3 mt-5 bread">Produk</h1>
		            <p class="breadcrumbs"><span class="mr-2"><a href="<?php echo site_url('index') ?>">Home</a></span> <span>Produk</span></p>
	            </div>

          	</div>
        </div>
     </div>
 </section>
    <section class="ftco-section">
    	<div class="container">
        <div class="row">
        	<div class="col-md-6 mb-5 pb-3">
        		<h3 class="mb-5 heading-pricing ftco-animate">Kopi Green Bean (OSE)</h3>
        		<?php foreach($produk as $p){ ?>
        			<?php if($p->nama_jenis == "Kopi Green Bean (OSE)"){ ?>
	        		<div class="pricing-entry d-flex ftco-animate">
	        			<div class="img" style="background-image: url(<?php echo base_url('public/images/produk/'.$p->gambar_produk) ?>);"></div>
	        			<div class="desc pl-3">
		        			<div class="d-flex text align-items-center">
		        				<h3><span><?php echo $p->nama_jenis ?> <?php echo $p->nama_olahan ?></span></h3>
		        				<span class="price">Rp <?php echo number_format($p->harga_produk,'0',',','.') ?></span>
		        			</div>
		        			<div class="d-block">
		        				<p><?php echo $p->detail_produk ?></p>
		        			</div>
	        			</div>
	        		</div>
        			<?php } ?>
        		<?php } ?>
        	</div>

        	<div class="col-md-6 mb-5 pb-3">
        		<h3 class="mb-5 heading-pricing ftco-animate">Kopi Sangrai</h3>
        		<?php foreach($produk as $p){ ?>
        			<?php if($p->nama_jenis == "Kopi Sangrai"){ ?>
	        		<div class="pricing-entry d-flex ftco-animate">
	        			<div class="img" style="background-image: url(<?php echo base_url('public/images/produk/'.$p->gambar_produk) ?>);"></div>
	        			<div class="desc pl-3">
		        			<div class="d-flex text align-items-center">
		        				<h3><span><?php echo $p->nama_jenis ?> <?php echo $p->nama_olahan ?></span></h3>
		        				<span class="price">Rp <?php echo number_format($p->harga_produk,'0',',','.') ?></span>
		        			</div>
		        			<div class="d-block">
		        				<p><?php echo $p->detail_produk ?></p>
		        			</div>
	        			</div>
	        		</div>
        			<?php } ?>
        		<?php } ?>
        	</div>

        	<div class="col-md-6">
        		<h3 class="mb-5 heading-pricing ftco-animate">Kopi Bubuk</h3>
        		<?php foreach($produk as $p){ ?>
        			<?php if($p->nama_jenis == "Kopi Bubuk"){ ?>
	        		<div class="pricing-entry d-flex ftco-animate">
	        			<div class="img" style="background-image: url(<?php echo base_url('public/images/produk/'.$p->gambar_produk) ?>);"></div>
	        			<div class="desc pl-3">
		        			<div class="d-flex text align-items-center">
		        				<h3><span><?php echo $p->nama_jenis ?> <?php echo $p->nama_olahan ?></span></h3>
		        				<span class="price">Rp <?php echo number_format($p->harga_produk,'0',',','.') ?></span>
		        			</div>
		        			<div class="d-block">
		        				<p><?php echo $p->detail_produk ?></p>
		        			</div>
	        			</div>
	        		</div>
        			<?php } ?>
        		<?php } ?>
        	</div>
        </div>
    	</div>
    </section>