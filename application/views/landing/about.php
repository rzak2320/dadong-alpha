<section class="home-slider owl-carousel" style="height: 500px">
      <div class="slider-item" style="background-image: url(<?php echo base_url() ?>public/front/images/bg_1.jpg); height: 450px" data-stellar-background-ratio="0.5">
      	<div class="overlay"></div>
        <div class="container">
          <div class="row slider-text justify-content-center align-items-center">

            <div class="col-md-7 col-sm-12 text-center ftco-animate">
            	<h1 class="mb-3 mt-5 bread">About Us</h1>
	            <p class="breadcrumbs"><span class="mr-2"><a href="<?php echo site_url('index') ?>">Home</a></span> <span>About</span></p>
            </div>

          </div>
        </div>
      </div>
    </section>

    <section class="ftco-about d-md-flex">
    	<div class="col-md-2"></div>
    	<div class="col-md-8 ftco-animate">
    		<div class="overlap">
		        <div class="heading-section ftco-animate ">
		        	<span class="subheading">Tentang</span>
		          <h2 class="mb-4">Kopi Dadong</h2>
		        </div>
	        	<div>
	  				<p>Kabupaten Bangli Provinsi Bali selain terkenal dengan keindahan alam dan pariwisatanya, salah satu komoditas asal Bali yang mendunia adalah kopi, terutama Kopi Arabika Kintamani, yang tergolong sebagai kopi premium spesial. Kintamani adalah kecamatan dengan wilayah terluas di Kabupaten Bangli, dan sekaligus penghasil kopi Arabika terbesar di Bali. Jumlah produksi kopi arabika di Bangli menunjukkan trend yang meningkat, meskipun luas lahan perkebun menyusut dan terbatas mengingat areal penanaman didominasi oleh perkebunan rakyat. Salah satu faktor yang menyebabkan meningkatnya hasil produksi adalah cuaca yang mendukung dan penguasaan teknis budidaya yang cukup baik dari para petani di Bangli, sehingga dapat mengoptimalkan lahan yang terbatas (intensifikasi).</p>
	  				<p>Kopi arabika kintamani merupakan salah satu kopi dengan cita rasa yang khas di Indonesia. Kopi Arabika tumbuh dengan subur di lereng Gunung Batur dengan konstur tanah vulkanis yang pertumbuhannya diantara ketinggian 900-1300 dpl. Pohon kopi yang ditumbuh diberikan penaung dari berbagai tanaman dan dapat ditumpangsarikan dengan jeruk. Pemupukan yang dilakukan secara organik untuk menghasilkan produksi yang baik. Pada awalnya petani kopi sebagian besar menjual kopi dalam bentuk biji (Glondong Merah/GM) dengan harga yang rendah dan belum berpikir dalam menciptakan kopi arabika kintamani yang spesial. Dengan adanya kesamaan citarasa beberapa daerah (letak Geografis) maka tahun 2018 keluarlah sertifikat Indikasi Geografis (IG) nomer. ID IG000001 dari Kementrian Hukum dan Ham. Indikasi Geografis adalah tanda mutu resmi yang menyatakan asal dan kekhasan produk. Dengan keluarnya Indikasi Geografis maka keluarlah SOP dalam pola budidaya dengan sebutan full wash atau olah basah giling kering untuk dapat menghasilkan kopi yang spesial.</p>
	  				<p>Dengan potensi kopi arabika Kintamani yang menasional, terlebih menjadi sajian kopi utama dalam hari kemerdekaan Republik Indonesia di Istana Negara tahun 2017 menyebabkan permintaan kopi arabika kintamani semakin melonjak. Adapun yang mengambil kesempatan dalam peluang bisnis ini adalah Ibu I Dewa Ayu Putri Asih Banjar.</p>
	  				<p>Semua bahan baku Gondong Merah “Kopi Bali” dihasilkan oleh potensi petani disebut Subak Abian mempunyai sejarah tradisi yang panjang. Mereka mengelola seacara demokratis urusan-urusan social/agama dan proyek-proyek kolektif di dusun-dusun Kawasan Kintamani. Lembaga Subak Abian dilandasi oleh azas “Tri Hita Karana”. Mengikuti azas ini tiga sebab untuk memperoleh kebahagiaan adalah hubungan yang harmonis antara manusia:
	  					<ul>
	  						<li>Dengan Tuhan (Parahyangan)</li>
	  						<li>Dengan sesama manusia (Pawongan)</li>
	  						<li>Dengan alam lingkungannya (Palemahan)</li>
	  					</ul>
	  				</p>
	  				<p>Kopi diproduksi dengan prinsip agama (adat) serta kepedulian lingkungan (secara Organik).</p>
	  				<p>Ibu I Dewa Ayu Putri Asih Banjar berasal dari Banjar Mabi Desa Belantih – Kintamani, Kab. Bangli merupakan adalah seorang Ibu Rumah Tangga yang kesehariannya melakukan pekerjaan di rumah dan ke ladang. Sejalan dengan terbentuknya Koperasi Petani Kopi MPIG, beliau diminta membantu koperasi sebagai bendahara. Dari bergabung dengan koperasi inilah terbentuk insting seorang wirasuha perkopian. Dengan adanya koperasi yang mewujudkan pemasaran satu pintu Ibu I Dewa Ayu Putri Asih Banjar mulai belajar bagaimana kualitas kopi yang baik dan berhubungan dengan beberapa kelompok Tani Kopi. Dirasakan sendiri bahwasanya petani kopi di daerahnya masih menjadi obyek dalam perdagangan kopi yang tidak terlalu menikmati hasil jerih payahnya karena masih menjual kopi glondong merah. Sejalan dengan berkembangnya koperasi yang fokus dalam penjualan kopi HS dilihatlah peluang dalam pengembangan produk hilirisasi yang belum banyak yang menggelutinya. Maka sejalan dengan tekad serta fasilitasi peralatan di koperasi untuk hilirisasi produk dapat diwujudkan mulailah Ibu I Dewa Ayu Putri Asih Banjar belajar bagaiman menciptakan kopi bubuk. Terlebih adanya kerjasama dengan pengusaha kopi dimana Ibu I Dewa Ayu Putri Asih Banjar diberikan kepercayaan untuk memproduksi kopi bubuk mulai dari pemilihan kopi, menyangrai kopi sampai dengan pembubukan. Dengan bekal kesempatan itulah Ibu I Dewa Ayu Putri Asih Banjar mulai memproduksi kopi sendiri dengan modal yang masih sangat minim. Pemasaran yang dilakukan hanyalah pertemanan dan ternyata usaha tersebut mulai berkembang dengan didukung penuh suami dan keluarga. Saat ini usaha pembuatan kopi bubuk mulai bervariasi dengan membuat beberapa pilihan pengolahan kopi diantaranya :
	  					<ul>
	  						<li>Nautral Kopi, yaitu kopi yang dipetik 100% glondong merah kemudian di jemur sampai kering dan siap dipasarkan</li>
	  						<li>Honey Kopi (Kopi Madu) adalah penyebutan petani apabila dalam pengolahannya kopi glondong merah dikupas langsung dijemur sehingga lendir pada kopi arabika yang manis akan menempel pada kopi yang dijemur</li>
	  						<li>Kopi FullWash (Olah Basang Giling Kering), merupakan standar SOP IG Kopi Arabika yang telah dilaksanakan oleh petani kopi seluruhnya. Adapun citarasa kopi arabika menurut keasaman yang dibentuk dalam prementasi yang dilakukan semakin kopi arabika kintamani. Rata-rata permentasi yang dilakukan antara 12 jam s.d 36 jam</li>
	  						<li>Kopi DP, sering disebut petani kopi untuk kopi bubuk tanpa grade dimana petani-petani kopi yang menjual kopinya secara acak dengan tercampur buah kopi hijau, kuning, merah dan hitam yang kemudian disangrai menjadi kopi DP dengan pangsa pasar masyarakat sekitarnya</li>
	  					</ul>
	  				</p>
	  				<p>Perjalanan usaha yang digelutinya membawa Ibu I Dewa Ayu Putri Asih Banjar menjadi salah satu pengusaha kopi industri kecil yang mulai dikenal di Daerah Kintamani. Berbagai tawaran kerjasama berdatangan dan sudah tentu menapatkan pembeli yang loyal dengan prouk kopi yang dihasilkan. Perjalanan dalam memproduksi kopi bubuk tetap dilakukan dengan mulai memperbaiki kemasan yang ada. Untuk legalisasi Hukum telah memperoleh PIRT di Kab. Bangli dengan Merk KOPI DADONG.</p>
	  				<p>Kopi Dadong sendiri mempunyai filosofi bahwa produksi yang dilakukan oleh Ibu I Dewa Ayu Putri Asih Banjar adalah memproduksi kopi secara tradisional dengan pengolahan yang sederhana seperti pengolahan yang dilakukan para ibu-ibu dan nenek-nenek di kampung. Namun kualitas dari buah kopi /Glondong kopi 90% merah tetap diutamakan untuk menghassilkan kopi yang berkualitas dengan citarasa yang baik.</p>
	  			</div>
  			</div>
    	</div>
    	<div class="col-md-2"></div>
    </section>	