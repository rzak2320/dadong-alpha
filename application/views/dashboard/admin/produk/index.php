<div class="content-wrapper">
	<section class="content-header">
		<h1>Daftar Produk Kopi Dadong</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<?php if($this->session->flashdata('status') == "gagal"){ ?>
				<div class="alert alert-danger"><?php echo $this->session->flashdata('message') ?></div>
				<?php } ?>
				<?php if($this->session->flashdata('status') == "berhasil"){ ?>
				<div class="alert alert-success"><?php echo $this->session->flashdata('message') ?></div>
				<?php } ?>
				<div class="box">
					<div class="box-header">
						<a href="#" data-toggle="modal" data-target="#tambah" class="btn btn-success" title="tambah"><i class="fa fa-plus"></i></a>
					</div>
					<div class="box-body">
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Produk</th>
									<th class="text-center">Harga</th>
									<th class="text-center"></th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach($produk as $p){ ?>
								<tr>
									<td class="text-center"><?php echo $no++ ?></td>
									<td><?php echo $p->nama_jenis."-".$p->nama_olahan ?></td>
									<td><?php echo number_format($p->harga_produk,'0',',','.') ?></td>
									<td class="text-center">
										<a href="#" class="btn btn-info btn-xs" title="edit"><i class="fa fa-edit"></i></a>
										<a href="#" data-url="" class="btn btn-danger btn-xs" title="hapus"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- Tambah -->
		<div id="tambah" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4>Tambah Produk</h4>
					</div>
					<div class="modal-body">
						<?php echo form_open_multipart('Admin/Produk/tambah') ?>
							<div class="form-group">
								<select style="width: 45%" name="jenis" id="select">
									<option>-- Pilih Jenis Produk --</option>
									<?php foreach($jenis as $j){ ?>
									<option value="<?php echo $j->id_jenis ?>"><?php echo $j->nama_jenis ?></option>
									<?php } ?>
								</select>
								<select style="width: 45%" name="olahan" id="select2">
									<option>-- Pilih Olahan Produk --</option>
									<?php foreach($olahan as $o){ ?>
									<option value="<?php echo $o->id_olahan ?>"><?php echo $o->nama_olahan ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<input type="text" name="produk" class="form-control" placeholder="Nama Produk" required>
							</div>
							<div class="form-group">
								<input type="text" name="harga" class="form-control" placeholder="Harga Produk" required>
							</div>
							<div class="form-group">
								<textarea name="detail" id="text"></textarea>
							</div>
							<div class="form-group">
								<input type="file" name="gambar" id="upload">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>