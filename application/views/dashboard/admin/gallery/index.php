<div class="content-wrapper">
	<section class="content-header">
		<h1>Kelola Gallery</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<?php if($this->session->flashdata('status') == "gagal"){ ?>
					<div class="alert alert-danger"><?php echo $this->session->flashdata('message') ?></div>
				<?php } ?>
				<?php if($this->session->flashdata('status') == "berhasil"){ ?>
					<div class="alert alert-success"><?php echo $this->session->flashdata('message') ?></div>
				<?php } ?>
				<div class="box">
					<div class="box-header">
						<a href="#" data-toggle="modal" data-target="#tambah" class="btn btn-success"><i class="fa fa-plus"></i></a>
					</div>
					<div class="box-body">
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Gambar</th>
									<th class="text-center">Keterangan</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach($gallery as $g){ ?>
								<tr>
									<td class="text-center"><?php echo $no++ ?></td>
									<td><img src="<?php echo base_url('public/images/gallery/'.$g->pict_gallery) ?>" width="50px" height="50px" alt="images"></td>
									<td><?php echo $g->nama_gallery ?></td>
									<td class="text-center">
										<a href="#" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
										<a href="#" class="btn btn-success btn-xs"><i class="fa fa-picture-o"></i></a>
										<a href="#" data-url="<?php echo site_url('Admin/Gallery/hapus/'.$g->id_gallery) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" role="dialog" id="tambah">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
						<h4>Tambah Gallery</h4>
					</div>
					<div class="modal-body">
						<?php echo form_open_multipart('Admin/Gallery/tambah') ?>
							<div class="form-group">
								<input type="file" name="gambar" id="upload">
							</div>
							<div class="form-group">
								<input type="text" name="nama" class="form-control" placeholder="Keterangan Gambar" required>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Upload</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
     	$('.confirm_delete').on('click', function(){
        
	        var delete_url = $(this).attr('data-url');

	        swal({
	          title: "Hapus Gallery",
	          text: "Yakin ingin menghapus ?",
	          type: "warning",
	          showCancelButton: true,
	          confirmButtonColor: "#FA041B",
	          confirmButtonText: "Hapus !",
	          cancelButtonText: "Batalkan",
	          closeOnConfirm: false     
	        }, function(){
	          window.location.href = delete_url;
	        });

	        return false;
      	});
    });
</script>