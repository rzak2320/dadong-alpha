<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; <?php echo date('Y') ?></strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->


<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url() ?>public/dashboard/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url() ?>public/dashboard/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>public/dashboard/dist/js/adminlte.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url() ?>public/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>public/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url() ?>public/plugins/select2/dist/js/select2.min.js"></script>
<!-- SweetAlert -->
<script src="<?php echo base_url() ?>public/plugins/sweetalert/dist/sweetalert.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url() ?>public/plugins/summernote/dist/summernote.min.js"></script>
<!-- Dropify -->
<script src="<?php echo base_url() ?>public/plugins/dropify/dist/js/dropify.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    // Kontrol Menu
    $('#nav a[href~="' + location.href + '"]').parents('li').addClass('active');
    // DataTable
    $('#table').DataTable();
    // Select2
    $('#select').select2();
    $('#select2').select2();
    // Summernote
    $('#text').summernote();
    // Dropify
    $('#upload').dropify();
  })
</script>  
</body>
</html>