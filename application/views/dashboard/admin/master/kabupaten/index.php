<div class="content-wrapper">
	<section class="content-header">
		<h1>Daftar Kecamatan</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-6">
				<div class="box">
					<div class="box-header">
						Tambah Kabupaten
					</div>
					<div class="box-body">
						<?php echo form_open('Admin/Kabupaten/tambah') ?>
							<div class="form-group">
								<select name="provinsi" id="select">
									<option>-- Pilih Provinsi --</option>
									<?php foreach($provinsi as $p){ ?>
									<option value="<?php echo $p->id_provinsi ?>"><?php echo $p->nama_provinsi ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<input type="text" name="kabupaten" class="form-control" placeholder="Nama Kabuapaten" required>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Kabupaten</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach($kabupaten as $k){ ?>
								<tr>
									<td class="text-center"><?php echo $no++ ?></td>
									<td><?php echo $k->nama_kabupaten ?></td>
									<td class="text-center">
										<a href="<?php echo site_url('Admin/Kabupaten/edit/'.$k->id_kabupaten) ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
										<a href="#" data-url="<?php echo site_url('Admin/Kabupaten/hapus/'.$k->id_kabupaten) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	// Hapus
    $(document).ready(function(){
     	$('.confirm_delete').on('click', function(){
        
	        var delete_url = $(this).attr('data-url');

	        swal({
	          title: "Hapus Kabupaten",
	          text: "Yakin ingin menghapus kabupaten ini ?",
	          type: "warning",
	          showCancelButton: true,
	          confirmButtonColor: "#FA041B",
	          confirmButtonText: "Hapus !",
	          cancelButtonText: "Batalkan",
	          closeOnConfirm: false     
	        }, function(){
	          window.location.href = delete_url;
	        });

	        return false;
      	});
    });
</script>