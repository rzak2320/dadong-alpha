<div class="content-wrapper">
	<section class="content-header">
		<h1>Edit Data Kabupaten</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<?php echo form_open('Admin/Kabupaten/update') ?>
							<div class="form-group">
								<select name="provinsi" id="select">
									<option value="0">-- Pilih Provinsi --</option>
									<?php foreach($provinsi as $p){ ?>
									<option value="<?php echo $p->id_provinsi ?>" <?php if($p->id_provinsi == $kabupaten['id_provinsi']){echo "selected";} ?>><?php echo $p->nama_provinsi ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="kabupaten" value="<?php echo $kabupaten['nama_kabupaten'] ?>" required>
							</div>
							<div class="form-group">
								<input type="hidden" name="id" class="form-group" value="<?php echo $kabupaten['id_kabupaten'] ?>" readonly>
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>