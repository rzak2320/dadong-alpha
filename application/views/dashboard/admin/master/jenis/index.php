<div class="content-wrapper">
	<section class="content-header">
		<h1>Data Master Jenis</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<a href="#" data-toggle="modal" data-target="#tambah" class="btn btn-success" title="tambah"><i class="fa fa-plus"></i></a>
					</div>
					<div class="box-body">
						<table class="table table-bordered table-stripped" id="table">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Jenis</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach($jenis as $j){ ?>
								<tr>
									<td class="text-center"><?php echo $no++ ?></td>
									<td><?php echo $j->nama_jenis ?></td>
									<td class="text-center">
										<a href="#" data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs" onclick="edit(<?php echo $j->id_jenis ?>)" title="edit"><i class="fa fa-edit"></i></a>
										<a href="#" data-url="<?php echo site_url('Admin/Jenis/hapus/'.$j->id_jenis) ?>" class="btn btn-danger btn-xs confirm_delete" title="hapus"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Tambah -->
	<div id="tambah" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4>Tambah Data Jenis</h4>
				</div>
				<div class="modal-body">
					<?php echo form_open('Admin/Jenis/tambah') ?>
						<div class="form-group">
							<input type="text" name="jenis" class="form-control" placeholder="Nama Jenis" required>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- Edit -->
	<div id="edit" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4>Edit Data Jenis</h4>
				</div>
				<div class="modal-body">
					<?php echo form_open('Admin/Jenis/update') ?>
						<div class="form-group">
							<input type="text" name="jenis" class="form-control" placeholder="Nama Jenis" id="nama" required>
						</div>
						<div class="form-group">
							<input type="hidden" name="id" class="form-control" id="unik" readonly>
							<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	//Edit
	function edit(idjenis){
        $.ajax({
            url:"<?php echo site_url('Admin/Jenis/edit');?>",
            type:"post",
            dataType: 'json',
            data:{id:idjenis},
            cache:false,
            success:function(result){
              $('#unik').val(result['id_jenis']);
              $('#nama').val(result['nama_jenis']);
            }
        });
     }
    // Hapus
    $(document).ready(function(){
     	$('.confirm_delete').on('click', function(){
        
	        var delete_url = $(this).attr('data-url');

	        swal({
	          title: "Hapus Jenis",
	          text: "Yakin ingin menghapus jenis ini ?",
	          type: "warning",
	          showCancelButton: true,
	          confirmButtonColor: "#FA041B",
	          confirmButtonText: "Hapus !",
	          cancelButtonText: "Batalkan",
	          closeOnConfirm: false     
	        }, function(){
	          window.location.href = delete_url;
	        });

	        return false;
      	});
    });
</script>