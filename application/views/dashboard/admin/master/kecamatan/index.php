<div class="content-wrapper">
	<section class="content-header">
		<h1>Daftar Kecamatan</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-6">
				<div class="box">
					<div class="box-body">
						<?php echo form_open('Admin/Kecamatan/tambah') ?>
							<div class="form-group">
								<select name="kabupaten" id="select">
									<option value="0">-- Pilih Kabupaten --</option>
									<?php foreach($kabupaten as $k){ ?>
									<option value="<?php echo $k->id_kabupaten ?>"><?php echo $k->nama_kabupaten ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<input type="text" name="kecamatan" class="form-control" placeholder="Nama Kecamatan" required>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Kecamatan</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach($kecamatan as $kc){ ?>
								<tr>
									<td class="text-center"><?php echo $no++ ?></td>
									<td><?php echo $kc->nama_kecamatan ?></td>
									<td class="text-center">
										<a href="<?php echo site_url('Admin/Kecamatan/edit/'.$kc->id_kecamatan) ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
										<a href="#" data-url="<?php echo site_url('Admin/Kecamatan/hapus/'.$kc->id_kecamatan) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>