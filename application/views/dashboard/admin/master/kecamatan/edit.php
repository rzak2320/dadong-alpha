<div class="content-wrapper">
	<section class="content-header">
		<h1>Edit Kecamatan</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<?php echo form_open('Admin/Kecamatan/update') ?>
							<div class="form-group">
								<select name="kabupaten" id="select">
									<option value="0">-- Pilih Kabupaten --</option>
									<?php foreach($kabupaten as $k){ ?>
									<option value="<?php echo $k->id_kabupaten ?>" <?php if($kecamatan['id_kabupaten'] == $k->id_kabupaten){echo "selected";} ?>><?php echo $k->nama_kabupaten ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<input type="text" name="kecamatan" class="form-control" value="<?php echo $kecamatan['nama_kecamatan'] ?>" required>
							</div>
							<div class="form-group">
								<input type="hidden" name="id" class="form-control" value="<?php echo $kecamatan['id_kecamatan'] ?>" readonly>
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
							</div>					
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>