<div class="content-wrapper">
	<section class="content-header">
		<h1>Daftar Provinsi</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-6">
				<div class="box">
					<div class="box-header">
						Tambah Provinsi
					</div>
					<div class="box-body">
						<?php echo form_open('Admin/Provinsi/tambah') ?>
							<div class="form-group">
								<input type="text" name="provinsi" class="form-control" placeholder="Nama Provinsi" required>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Provinsi</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach($provinsi as $p){ ?>
								<tr>
									<td class="text-center"><?php echo $no++ ?></td>
									<td><?php echo $p->nama_provinsi ?></td>
									<td class="text-center">
										<a href="#" data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs" onclick="edit(<?php echo $p->id_provinsi ?>)"><i class="fa fa-edit"></i></a>
										<a href="#" data-url="<?php echo site_url('Admin/Provinsi/hapus/'.$p->id_provinsi) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- Edit -->
		<div id="edit" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4>Edit Provinsi</h4>
					</div>
					<div class="modal-body">
						<?php echo form_open('Admin/Provinsi/update') ?>
							<div class="form-group">
								<input type="text" name="provinsi" class="form-control" id="provinsi" required>
							</div>
							<div class="form-group">
								<input type="hidden" name="id" class="form-control" id="unik" readonly>
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	//Edit
	function edit(idprovinsi){
        $.ajax({
            url:"<?php echo site_url('Admin/Provinsi/edit');?>",
            type:"post",
            dataType: 'json',
            data:{id:idprovinsi},
            cache:false,
            success:function(result){
              $('#unik').val(result['id_provinsi']);
              $('#provinsi').val(result['nama_provinsi']);
            }
        });
     }
    // Hapus
    $(document).ready(function(){
     	$('.confirm_delete').on('click', function(){
        
	        var delete_url = $(this).attr('data-url');

	        swal({
	          title: "Hapus Provinsi",
	          text: "Yakin ingin menghapus provinsi ini ?",
	          type: "warning",
	          showCancelButton: true,
	          confirmButtonColor: "#FA041B",
	          confirmButtonText: "Hapus !",
	          cancelButtonText: "Batalkan",
	          closeOnConfirm: false     
	        }, function(){
	          window.location.href = delete_url;
	        });

	        return false;
      	});
    });
</script>