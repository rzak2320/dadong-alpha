<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_auth');
	}

	public function login_admin()
	{
		$user = $this->input->post('username');
		$password = $this->input->post('password');

		$cek = $this->M_auth->cek_user($user,'user');
		if($cek->num_rows() > 0){
			$row = $cek->row();
			if(password_verify($password,$row->password)){
				$session = array(
					'id' => $row->id_user,
					'nama' => $row->nama_user,
					'username' => $row->username,
					'email' => $row->email,
					'path' => $row->path,
					'akses' => $row->akses,
					'logged_in' => TRUE,
				);
				$this->session->set_userdata($session);
				redirect('Dashboard/admin');
			}else{
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf Password salah, silahkan cek kembali",
				);
				$this->session->set_flashdata($notif);
				redirect('admin');
			}
		}else{
			$notif = array(
				'status' => "gagal",
				'message' => "Maaf User ID ".$user." Tidak Terdaftar Silahkan Cek Kembali",
			);
			$this->session->set_flashdata($notif);
			redirect('admin');
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('index');
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */