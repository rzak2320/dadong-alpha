<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provinsi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('akses') != "admin"){
			if($this->session->userdata('logged_in') != TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('login');
			}else{
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf anda tidak diijinkan untuk mengakses",
				);
				$this->session->set_flashdata($notif);
				redirect('Dashboard');
			}
		}else{
			if($this->session->userdata('logged_in') != TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('admin');
			}
		}
		$this->load->model('M_provinsi');
	}

	public function index()
	{
		$data['provinsi'] = $this->M_provinsi->list_provinsi('provinsi')->result();

		$this->load->view('dashboard/admin/sidebar');
		$this->load->view('dashboard/admin/master/provinsi/index',$data);
		$this->load->view('dashboard/admin/footer');
	}

	public function tambah(){
		$data = array('nama_provinsi' => $this->input->post('provinsi'));

		$this->M_provinsi->create($data,'provinsi');
		redirect('Admin/Provinsi');
	}

	public function edit(){
		$id = $this->input->post('id');

		$result = $this->M_provinsi->get($id,'provinsi');
		echo json_encode($result);
	}

	public function update(){
		$where = array('id_provinsi' => $this->input->post('id'));
		$data = array('nama_provinsi' => $this->input->post('provinsi'));

		$this->M_provinsi->replace($where,$data,'provinsi');
		redirect('Admin/Provinsi');
	}

	public function hapus($id){
		$where = array('id_provinsi' => $id);

		$this->M_provinsi->trash($where,'provinsi');
		redirect('Admin/Provinsi');
	}

}

/* End of file Provinsi.php */
/* Location: ./application/controllers/Provinsi.php */