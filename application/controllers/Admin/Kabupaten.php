<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kabupaten extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('akses') != "admin"){
			if($this->session->userdata('logged_in') != TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('login');
			}else{
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf anda tidak diijinkan untuk mengakses",
				);
				$this->session->set_flashdata($notif);
				redirect('Dashboard');
			}
		}else{
			if($this->session->userdata('logged_in') != TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('admin');
			}
		}
		$this->load->model('M_kabupaten');
		$this->load->model('M_provinsi');
	}

	public function index()
	{
		$data = array(
			'kabupaten' => $this->M_kabupaten->list_kabupaten('kabupaten')->result(),
			'provinsi' => $this->M_provinsi->list_provinsi('provinsi')->result(),
		);

		$this->load->view('dashboard/admin/sidebar');
		$this->load->view('dashboard/admin/master/kabupaten/index',$data);
		$this->load->view('dashboard/admin/footer');
	}

	public function tambah(){
		$data = array(
			'id_provinsi' => $this->input->post('provinsi'),
			'nama_kabupaten' => $this->input->post('kabupaten'),
		);

		$this->M_kabupaten->create($data,'kabupaten');
		redirect('Admin/Kabupaten');
	}

	public function edit($id){
		$where = array('id_kabupaten' => $id);
		$data = array(
			'kabupaten' => $this->M_kabupaten->get($where,'kabupaten'),
			'provinsi' => $this->M_provinsi->list_provinsi('provinsi')->result(),
		);

		$this->load->view('dashboard/admin/sidebar');
		$this->load->view('dashboard/admin/master/kabupaten/edit',$data);
		$this->load->view('dashboard/admin/footer');
	}

	public function update(){
		$where = array('id_kabupaten' => $this->input->post('id'));
		$data = array(
			'nama_kabupaten' => $this->input->post('kabupaten'),
			'id_provinsi' => $this->input->post('provinsi'),
		);

		$this->M_kabupaten->replace($where,$data,'kabupaten');
		redirect('Admin/Kabupaten');
	}

	public function hapus($id){
		$where = array('id_kabupaten' => $id);

		$this->M_kabupaten->trash($where,'kabupaten');
		redirect('Admin/Kabupaten');
	}

}

/* End of file Kabupaten.php */
/* Location: ./application/controllers/Kabupaten.php */