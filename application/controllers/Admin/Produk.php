<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('akses') != "admin"){
			if($this->session->userdata('logged_in') != TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('login');
			}else{
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf anda tidak diijinkan untuk mengakses",
				);
				$this->session->set_flashdata($notif);
				redirect('Dashboard');
			}
		}else{
			if($this->session->userdata('logged_in') != TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('admin');
			}
		}
		$this->load->model('M_produk');
		$this->load->model('M_jenis');
		$this->load->model('M_olahan');
	}

	public function index()
	{
		$data = array(
			'produk' => $this->M_produk->list_produk('produk')->result(),
			'jenis' => $this->M_jenis->list_jenis('jenis')->result(),
			'olahan' => $this->M_olahan->list_olahan('olahan')->result(),
		);
		$this->load->view('dashboard/admin/sidebar');
		$this->load->view('dashboard/admin/produk/index',$data);
		$this->load->view('dashboard/admin/footer');
	}

	public function tambah(){
		$img = $_FILES['gambar']['name'];
		$config['upload_path'] = './public/images/produk/';
		$config['file_name'] = $this->input->post('jenis')."_".$this->input->post('olahan');
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size'] = 1024;
		$config['overwrite'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$config['file_ext_tolower'] = TRUE;

		$this->load->library('upload',$config);

		if(!$this->upload->do_upload('gambar')){
			$notif = array(
				'status' => "gagal",
				'message' => $this->upload->display_errors(),
			);
			$this->session->set_flashdata($notif);
			redirect('Admin/Produk');
		}else{
			$data = array(
				'id_jenis' => $this->input->post('jenis'),
				'id_olahan' => $this->input->post('olahan'),
				'nama_produk' => $this->input->post('produk'),
				'harga_produk' => $this->input->post('harga'),
				'detail_produk' => $this->input->post('detail'),
				'gambar_produk' => $this->upload->data('file_name'),
			);
			$notif = array(
				'status' => "berhasil",
				'message' => "Produk berhasil ditambahkan",
			);

			$this->M_produk->create($data,'produk');
			$this->session->set_flashdata($notif);
			redirect('Admin/Produk');
		}
	}

}

/* End of file Produk.php */
/* Location: ./application/controllers/Produk.php */