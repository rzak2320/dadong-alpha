<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Olahan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('akses') != "admin"){
			if($this->session->userdata('logged_in') != TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('login');
			}else{
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf anda tidak diijinkan untuk mengakses",
				);
				$this->session->set_flashdata($notif);
				redirect('Dashboard');
			}
		}else{
			if($this->session->userdata('logged_in') != TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('admin');
			}
		}
		$this->load->model('M_olahan');
	}

	public function index()
	{
		$data = array('olahan' => $this->M_olahan->list_olahan('olahan')->result());
		$this->load->view('dashboard/admin/sidebar');
		$this->load->view('dashboard/admin/master/olahan/index',$data);
		$this->load->view('dashboard/admin/footer');
	}

	public function tambah(){
		$data = array('nama_olahan' => $this->input->post('olahan'));

		$this->M_olahan->create($data,'olahan');
		redirect('Admin/Olahan');
	}

	public function edit(){
		$id = $this->input->post('id');
		$result = $this->M_olahan->ambil($id,'olahan');
		echo json_encode($result);
	}

	public function update(){
		$where = array('id_olahan' => $this->input->post('id'));
		$data = array('nama_olahan' => $this->input->post('olahan'));

		$this->M_olahan->replace($where,$data,'olahan');
		redirect('Admin/Olahan');
	}

	public function hapus($id){
		$where = array('id_olahan' => $id);
		$this->M_olahan->trash($where,'olahan');
		redirect('Admin/Olahan');
	}

}

/* End of file Olahan.php */
/* Location: ./application/controllers/Olahan.php */