<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('akses') != "admin"){
			if($this->session->userdata('logged_in') != TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('login');
			}else{
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf anda tidak diijinkan untuk mengakses",
				);
				$this->session->set_flashdata($notif);
				redirect('Dashboard');
			}
		}else{
			if($this->session->userdata('logged_in') != TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('admin');
			}
		}
		$this->load->model('M_gallery');
	}

	public function index()
	{
		$data['gallery'] = $this->M_gallery->list_gallery('gallery')->result();

		$this->load->view('dashboard/admin/sidebar');
		$this->load->view('dashboard/admin/gallery/index',$data);
		$this->load->view('dashboard/admin/footer');
	}

	function tambah(){
		$img = $_FILES['gambar']['name'];
		$config['upload_path'] = './public/images/gallery/';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['file_name'] = $this->input->post('nama');
		$config['overwrite'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$config['file_ext_tolower'] =TRUE;

		$this->load->library('upload',$config);

		if(!$this->upload->do_upload('gambar')){
			$notif = array(
				'status' => "gagal",
				'message' => $this->upload->display_errors(),
			);
			$this->session->set_flashdata($notif);
			redirect('Admin/Gallery');
		}else{
			$data = array(
				'pict_gallery' => $this->upload->data('file_name'),
				'nama_gallery' => $this->input->post('nama'),
			);
			$notif = array(
				'status' => "berhasil",
				'message' => "Gallery berhasil ditambahkan",
			);

			$this->M_gallery->create($data,'gallery');
			$this->session->set_flashdata($notif);

			redirect('Admin/Gallery');
		}
	}

	function hapus($id){
		$where = array('id_gallery' => $id);
		$this->M_gallery->trash($where,'gallery');
		redirect('Admin/Gallery');
	}

}

/* End of file Gallery.php */
/* Location: ./application/controllers/Gallery.php */