<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kecamatan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('akses') != "admin"){
			if($this->session->userdata('logged_in') != TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('login');
			}else{
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf anda tidak diijinkan untuk mengakses",
				);
				$this->session->set_flashdata($notif);
				redirect('Dashboard');
			}
		}else{
			if($this->session->userdata('logged_in') != TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('admin');
			}
		}
		$this->load->model('M_kecamatan');
		$this->load->model('M_kabupaten');
	}

	public function index()
	{
		$data = array(
			'kecamatan' => $this->M_kecamatan->list_kecamatan('kecamatan')->result(),
			'kabupaten' => $this->M_kabupaten->list_kabupaten('kabupaten')->result(),
		);

		$this->load->view('dashboard/admin/sidebar');
		$this->load->view('dashboard/admin/master/kecamatan/index',$data);
		$this->load->view('dashboard/admin/footer');
	}

	public function tambah(){
		$data = array(
			'id_kabupaten' => $this->input->post('kabupaten'),
			'nama_kecamatan' => $this->input->post('kecamatan'),
		);

		$this->M_kecamatan->create($data,'kecamatan');
		redirect('Admin/Kecamatan');
	}

	public function edit($id){
		$where = array('id_kecamatan' => $id);
		$data = array(
			'kecamatan' => $this->M_kecamatan->get($where,'kecamatan'),
			'kabupaten' => $this->M_kabupaten->list_kabupaten('kabupaten')->result(),
		);

		$this->load->view('dashboard/admin/sidebar');
		$this->load->view('dashboard/admin/master/kecamatan/edit',$data);
		$this->load->view('dashboard/admin/footer');
	}

	public function update(){
		$where = array('id_kecamatan' => $this->input->post('id'));
		$data = array(
			'id_kabupaten' => $this->input->post('kabupaten'),
			'nama_kecamatan' => $this->input->post('kecamatan'),
		);

		$this->M_kecamatan->replace($where,$data,'kecamatan');
		redirect('Admin/Kecamatan');
	}

}

/* End of file Kecamatan.php */
/* Location: ./application/controllers/Kecamatan.php */