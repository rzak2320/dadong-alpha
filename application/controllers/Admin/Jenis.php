<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jenis extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('akses') != "admin"){
			if($this->session->userdata('logged_in') != TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('login');
			}else{
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf anda tidak diijinkan untuk mengakses",
				);
				$this->session->set_flashdata($notif);
				redirect('Dashboard');
			}
		}else{
			if($this->session->userdata('logged_in') != TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('admin');
			}
		}
		$this->load->model('M_jenis');
	}

	public function index()
	{
		$data = array('jenis' => $this->M_jenis->list_jenis('jenis')->result());
		$this->load->view('dashboard/admin/sidebar');
		$this->load->view('dashboard/admin/master/jenis/index',$data);
		$this->load->view('dashboard/admin/footer');
	}

	public function  tambah(){
		$data = array('nama_jenis' => $this->input->post('jenis'));

		$this->M_jenis->create($data,'jenis');
		redirect('Admin/Jenis');
	}

	public function edit(){
		$id = $this->input->post('id');
		$result = $this->M_jenis->ambil($id,'jenis');
		echo json_encode($result);
	}


	public function update(){
		$where = array('id_jenis' => $this->input->post('id'));
		$data = array('nama_jenis' => $this->input->post('jenis'));

		$this->M_jenis->replace($where,$data,'jenis');
		redirect('Admin/Jenis');
	}

	public function hapus($id){
		$where = array('id_jenis' => $id);

		$this->M_jenis->trash($where,'jenis');
		redirect('Admin/Jenis');
	}

}

/* End of file Jenis.php */
/* Location: ./application/controllers/Jenis.php */