<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

	}

	public function admin()
	{
		if($this->session->userdata('logged_in') != TRUE){
			$notif = array(
				'status' => "gagal",
				'message' => "Silahkan login terlebih dahulu",
			);
			$this->session->set_flashdata($notif);
			redirect('admin');
		}elseif($this->session->userdata('akses') != "admin"){
			if($this->session->userdata('logged_in') == TRUE){
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf anda tidak memiliki ijin untuk mengakses",
				);
				$this->session->set_flashdata($notif);
				redirect('Dashboard');
			}else{
				$notif = array(
					'status' => "gagal",
					'message' => "Silahkan login terlebih dahulu",
				);
				$this->session->set_flashdata($notif);
				redirect('index');
			}
		}else{
			$this->load->view('dashboard/admin/sidebar');
			$this->load->view('dashboard/admin/index');
			$this->load->view('dashboard/admin/footer');
		}		
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */