<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_gallery');
	}

	public function index()
	{
		$data['gallery'] = $this->M_gallery->list_gallery('gallery')->result();
		$this->load->view('landing/header');
		$this->load->view('landing/index',$data);
		$this->load->view('landing/footer');	
	}

	public function produk(){
		$this->load->model('M_produk');

		$data['produk'] = $this->M_produk->produk_pajang()->result();
		$this->load->view('landing/header');
		$this->load->view('landing/produk',$data);
		$this->load->view('landing/footer');
	}

	public function about(){
		$this->load->view('landing/header');
		$this->load->view('landing/about');
		$this->load->view('landing/footer');
	}

	public function contact(){
		$this->load->view('landing/header');
		$this->load->view('landing/contact');
		$this->load->view('landing/footer');
	}

	public function admin(){
		$this->load->view('login');
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */